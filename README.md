# flutterchallenge App

This application uses the `Bloc cubit` architecture .Each view has it's own model, any widget with logic would have the same, and we use services to define all our business logic. 

## Screenshots
<p>
<img src="media/1.png" width="200"/> <img src="media/2.png" width="200"/> <img src="media/3.png" width="200"/> <img src="media/4.png" width="200"/> 

<img src="media/5.png" width="200"/> <img src="media/6.png" width="200"/> <img src="media/8.png" width="200"/> <img src="media/9.png" width="200"/> 
</p>

## How to use


Step 1:

Download or clone this repository with the following command

```
git clone https://gitlab.com/UnluckyY1/flutterchallenge.git
```


Step 2:

At the root of the project run the following command in the console to get the necessary dependencies:

```
flutter pub get
```

Step 3:

At the root of the project run the following command in the console to run the app:

```
flutter run --release
```


## Used Libraries & Packages


- `dio: ^4.0.0`: Allows to make http requests
- `flutter_bloc: ^7.3.0`: state mangment
- `connectivity: ^3.0.6 `: To check internet connection
- `hive: ^2.0.4`: to save user preference (like theme)
- `sqflite`:  loacal database
- `path_provider`: For accessing to the local database
- `google_fonts: ^2.1.0`: for application font
- `font_awesome_flutter: ^9.1.0`: for ui icons
- `url_launcher: ^6.0.10`: allow user to send emails / call Recipients

## Characteristics

- The application has been structured following Clean Architecture
- Consumption of an Api Rest
- Showing the data from local database
- Display information in a beautiful  interface



## Project structure

The lib folder is divided into two folders. core and ui. Core contains all the files associated with the logic. ui contains all the files associated with the ui.

Core is divided into 4 folders.

- consts: Contains const keys values
- Models: Contains all the plain data models
- Services: Contains the dedicated files that will handle actual business logic
- Cubit: Contains the cubit models for each of the Widget views.

UI is divided into 3 folders 


- Styles: Contains all style files like theme
- utils: Contains the utility files needed for UI (like extensions)
- view: Contains all files of the actual ui

