import 'package:flutter/material.dart';
import 'package:oolab_test/UI/View/Widgets/page_template.dart';
import 'package:oolab_test/UI/View/Widgets/space.dart';
import 'package:oolab_test/UI/View/Widgets/title_text.dart';
import 'package:oolab_test/UI/View/settings/theme_toggle_list.dart';
import 'package:oolab_test/UI/styles/values.dart';
import 'package:oolab_test/UI/utils/extensions.dart';


class MorePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PageTemplate(children: [
      Column(children: [
        TitleText(title: "More".toUpperCase()),

        Spacing(height: Values.marginBelowTitle / 2),

        ThemeToggleList()
       
      ]).sliverToBoxAdapter()
    ]).scaffold();
  }
}
