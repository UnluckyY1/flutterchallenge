import 'package:flutter/material.dart';
import 'package:oolab_test/UI/View/Widgets/space.dart';
import 'package:oolab_test/UI/styles/values.dart';

class PageTemplate extends StatelessWidget {
  final List children;

  final bool overscroll;

  PageTemplate({this.children = const [], this.overscroll = true});

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return SafeArea(
      top: false,
      bottom: false,
      child: Container(
        padding: EdgeInsets.only(
          // top: statusBarHeight,
          left: Values.pageHorizontalPadding,
          right: Values.pageHorizontalPadding,
        ),
        child: CustomScrollView(
          slivers: <Widget>[
            Spacing(height: statusBarHeight).sliver(),

            Spacing(height: 20).sliver(),

            ...children,

            // allow for some overscroll
            // this is a feature, not a bug
            if (overscroll) ...[
              Spacing(height: 240).sliver(),
            ]
          ],
        ),
      ),
    );
  }

  Widget scaffold() {
    return Scaffold(
      body: this,
    );
  }
}
