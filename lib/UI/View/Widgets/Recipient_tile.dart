import 'package:flutter/material.dart';
import 'package:oolab_test/Core/model/Recipients.dart';
import 'package:oolab_test/UI/styles/tile_color.dart';
import 'package:oolab_test/UI/styles/values.dart';
import 'package:oolab_test/UI/utils/url.dart';

class RecipientTile extends StatelessWidget {
  final Recipients recipient;

  const RecipientTile({Key? key, required this.recipient}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Values.marginBelowTitle),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Values.tileHorizontalPadding,
          vertical: Values.tileVerticalPadding,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // make sure not to get the overflow error
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.60,
                  child: Text(
                    "${recipient.civility} ${recipient.firstname} ${recipient.lastname}",
                    maxLines: 2,
                    style: Theme.of(context).textTheme.headline4,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Text(
                    "${recipient.address1} ${recipient.postalCode} ${recipient.city} ",
                    style: Theme.of(context).textTheme.headline3)
              ],
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.mail,
                    ),
                    onPressed: () => openmail(recipient.email),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.phone,
                    ),
                    onPressed: () => openphonecall(recipient.cellPhone),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
        color: TileColors.serviceTile(context),
        borderRadius: BorderRadius.circular(Values.borderRadius),
      ),
    );

    // ListTile seems to have the padding I want without me setting it ...
  }
}
