import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {
  final String title;
  final IconData? iconData;
  final Function()? onPress;

  TitleText({required this.title, this.iconData, this.onPress});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Flexible(
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline6,
            textAlign: TextAlign.start,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        onPress != null
            ? IconButton(
                onPressed: () {
                  onPress!();
                },
                icon: Icon(
                  iconData,
                  size: Theme.of(context).textTheme.headline6!.fontSize! / 1.2,
                  color: Theme.of(context).textTheme.headline6!.color,
                ),
              )
            : Icon(
                iconData,
                size: Theme.of(context).textTheme.headline6!.fontSize! / 1.2,
                color: Theme.of(context).textTheme.headline6!.color,
              )
      ],
    );
  }
}
