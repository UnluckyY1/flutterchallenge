import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:oolab_test/Core/cubit/Home_cubit/Home_cubit.dart';
import 'package:oolab_test/UI/View/Home/widget/add_recipient.dart';
import 'package:oolab_test/UI/View/Home/widget/recipients_list.dart';
import 'package:oolab_test/UI/View/Widgets/page_template.dart';
import 'package:oolab_test/UI/View/Widgets/space.dart';
import 'package:oolab_test/UI/View/Widgets/title_text.dart';
import 'package:oolab_test/UI/styles/values.dart';
import 'package:oolab_test/UI/utils/extensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    context.read<HomeCubit>().fetchdata();
    return RefreshIndicator(
        child: PageTemplate(
          children: [
            TitleText(
              title: "Home".toUpperCase(),
              iconData: FontAwesomeIcons.userPlus,
              onPress: () => AddRecipientWidget.showAddMemberDialog(context),
            ).sliverToBoxAdapter(),

            // search bar floats on top when you scroll down then scroll back up

            Spacing(height: Values.marginBelowTitle).sliver(),

            RecipientsList().sliverToBoxAdapter()
          ],
        ).scaffold(),
        onRefresh: () => context.read<HomeCubit>().refrechdata());
  }
}
