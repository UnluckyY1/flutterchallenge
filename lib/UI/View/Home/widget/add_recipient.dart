import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oolab_test/Core/Services/Recipients_repository.dart';
import 'package:oolab_test/Core/cubit/Create_cubit/Create_cubit.dart';
import 'package:oolab_test/Core/cubit/Home_cubit/Home_cubit.dart'
    hide Error, Loading;
import 'package:oolab_test/UI/View/Widgets/AlertDialog.dart';
import 'package:oolab_test/UI/View/Widgets/ProgressIndicatorWidget.dart';
import 'package:oolab_test/UI/utils/route.dart';

class AddRecipientWidget extends StatefulWidget {
  static showAddMemberDialog(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return BlocProvider(
            create: (context) => CreateCubit(Repository()),
            child: Center(
              child: AddRecipientWidget(),
            ),
          );
        });
  }

  @override
  _AddRecipientWidgetState createState() => _AddRecipientWidgetState();
}

class _AddRecipientWidgetState extends State<AddRecipientWidget> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  final inputDecoration = InputDecoration(
    contentPadding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 10.0),
  );
  String? textCivilty = 'MONSIEUR';
  final textFirst = TextEditingController();
  final textLast = TextEditingController();
  final textEmail = TextEditingController();
  final textPhone = TextEditingController();
  final textCity = TextEditingController();
  final textZip = TextEditingController();
  final textAdress1 = TextEditingController();
  final textAdress2 = TextEditingController();

  String? validate(String value) {
    if (value.isEmpty) {
      return ('utils.inputError');
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 8.0, right: 8.0, left: 8.0),
        child: Material(
          child: Form(
            key: _formKey,
            // ignore: deprecated_member_use
            autovalidate: _autoValidate,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Civility :"),
                        DropdownButton<String>(
                          value: textCivilty,
                          items: <String>['MONSIEUR', 'MADAME']
                              .map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              textCivilty = value;
                            });
                          },
                        )
                      ],
                    )),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          controller: textFirst,
                          decoration: inputDecoration.copyWith(
                            labelText: "First Name*",
                          ),
                          validator: validateFirstName,
                        ),
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          controller: textLast,
                          decoration: inputDecoration.copyWith(
                              labelText: "${('Last Name')} *"),
                          validator: validateLastName,
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: textEmail,
                    decoration: inputDecoration.copyWith(
                      labelText: "Email *",
                    ),
                    validator: validateEmail,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.phone,
                    controller: textPhone,
                    decoration: inputDecoration.copyWith(
                      labelText: "Phone *",
                    ),
                    validator: validatePhone,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          controller: textCity,
                          decoration: inputDecoration.copyWith(
                            labelText: ('City'),
                          ),
                        ),
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          controller: textZip,
                          decoration:
                              inputDecoration.copyWith(labelText: ('Zip code')),
                          validator: validateZip,
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: textAdress1,
                    decoration: inputDecoration.copyWith(
                      labelText: "Adress 1 ",
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: textAdress2,
                    decoration: inputDecoration.copyWith(
                      labelText: "Adress 2 ",
                    ),
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                BlocConsumer<CreateCubit, CreateState>(
                  listener: (context, state) {
                    if (state is Created) {
                      Routing.closeRoute(context);
                      context.read<HomeCubit>().refrechdata();
                    }
                    if (state is Error) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return RichAlertDialog(
                              alertTitle: Text(
                                "Somthing went wrong !",
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              alertSubtitle: richSubtitle(state.message),
                              alertType: RichAlertType.ERROR,
                              actions: <Widget>[
                                // ignore: deprecated_member_use
                                FlatButton(
                                  child: Text("try again"),
                                  onPressed: () {
                                    Routing.closeRoute(context);
                                  },
                                ),
                              ],
                            );
                          });
                    }
                  },
                  builder: (context, state) {
                    if (state is Loading)
                      return ProgressIndicatorWidget();
                    else
                      return Padding(
                          padding: const EdgeInsets.all(5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                // ignore: deprecated_member_use
                                child: FlatButton(
                                  onPressed: () => Navigator.pop(context),
                                  color: Colors.white,
                                  textColor: theme.primaryColor,
                                  child: Text('cancel'),
                                ),
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              Expanded(
                                flex: 1,
                                // ignore: deprecated_member_use
                                child: RaisedButton(
                                  onPressed: () => _processData(context),
                                  color: theme.primaryColorDark,
                                  textColor: Colors.white,
                                  child: Text(
                                    ('Save'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ),
                              )
                            ],
                          ));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _processData(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      final Map<String, Object> data = new Map<String, Object>();
      data['civility'] = textCivilty!;
      data['firstname'] = textFirst.text;
      data['lastname'] = textLast.text;
      data['address_1'] = textAdress1.text;
      data['address_2'] = textAdress2.text;
      data['postal_code'] = textZip.text;
      data['city'] = textCity.text;
      data['email'] = textEmail.text;
      data['cell_phone'] = textPhone.text;

      await context.read<CreateCubit>().createRecipient(data);
      // _membersProvider.addMember(context, member: member);
    }
  }

  String? validateCivilty(String? value) {
    if (value!.isEmpty)
      return ('This filed cannot be empty  ');
    else
      return null;
  }

  String? validateFirstName(String? value) {
    if (value!.length < 3)
      return ('Enter a valid name ');
    else
      return null;
  }

  String? validateLastName(String? value) {
    if (value!.length < 2)
      return ('Enter a valid last name');
    else
      return null;
  }

  String? validatePhone(String? value) {
    if (value!.isEmpty)
      return 'Enter a valid phone number';
    else
      return null;
  }

  String? validateEmail(String? value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value!))
      return 'Enter Valid Email';
    else
      return null;
  }

  String? validateZip(String? value) {
    if (value!.isNotEmpty) {
      if (value.length < 3) return 'Zip code must be at least 4 digits';
    }
    return null;
  }
}
