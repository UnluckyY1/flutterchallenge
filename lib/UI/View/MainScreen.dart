import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oolab_test/Core/cubit/Intial_cubit/Main_cubit.dart';
import 'package:oolab_test/UI/View/Widgets/AlertDialog.dart';
import 'package:oolab_test/UI/View/Widgets/ProgressIndicatorWidget.dart';
import 'package:oolab_test/UI/View/tabbed_app.dart';

import 'package:oolab_test/UI/utils/route.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  void initState() {
    super.initState();
    context.read<MainCubit>().fetchdata();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<MainCubit, AppState>(
        listener: (context, state) {
          if (state is Loaded) {
            Routing.openReplacementRoute(context, TabbedApp());
          }
          if (state is Error) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return RichAlertDialog(
                    alertTitle: Text(
                      "Somthing went wrong !",
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    alertSubtitle: richSubtitle(state.message),
                    alertType: RichAlertType.ERROR,
                    actions: <Widget>[
                      // ignore: deprecated_member_use
                      FlatButton(
                        child: Text("try again"),
                        onPressed: () {
                          context.read<MainCubit>().fetchdata();
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                });
          }
        },
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Center(
                child: SvgPicture.asset(
                  "assets/oodrive_sign.svg",
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                ),
              ),
              ProgressIndicatorWidget(),
            ],
          );
        },
      ),
    );
  }
}
