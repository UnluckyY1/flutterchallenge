import 'package:flutter/material.dart';
import 'package:oolab_test/Core/cubit/Search_cubit/Search_cubit.dart';
import 'package:oolab_test/UI/View/Widgets/space.dart';
import 'package:oolab_test/UI/styles/text_fields.dart';
import 'package:oolab_test/UI/styles/values.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class SearchTextBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Spacing(height: Values.marginBelowTitle),
          Expanded(
            flex: 5,
            child: TextField(
              decoration: InputDecoration(
                hintText: "Type a Recipients first name or last name or email",
                filled: true,
                fillColor: TextFieldStyles.backgroundColor(context),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(Values.borderRadius),
                  borderSide: BorderSide(
                    color: TextFieldStyles.backgroundColor(context),
                    width: 0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(Values.borderRadius),
                    borderSide: BorderSide(
                      color: Theme.of(context).primaryColor,
                      width: 4,
                    )),
              ),
              onChanged: (query) => _onQueryChanged(context, query),
            ),
          ),
        ],
      ),
    );
  }

  _onQueryChanged(BuildContext context, String query) {
    //final SearchProvider searchProvider = Provider.of<SearchProvider>(context, listen: false);
    if (query.isNotEmpty) {
      context.read<SearchCubit>().searchRecipients(query);
    }
  }
}
