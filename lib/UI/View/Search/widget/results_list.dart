import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oolab_test/Core/cubit/Search_cubit/Search_cubit.dart';
import 'package:oolab_test/UI/View/Widgets/ProgressIndicatorWidget.dart';
import 'package:oolab_test/UI/View/Widgets/Recipient_tile.dart';
import 'package:oolab_test/UI/styles/tile_color.dart';
import 'package:oolab_test/UI/styles/values.dart';

class SearchResultsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchCubit, SearchState>(
      builder: (context, state) {
        if (state is Initial) return nothingSearched(context);
        if (state is Searching) return ProgressIndicatorWidget();
        if (state is Loaded) {
          if (state.list.isNotEmpty)
            return ListView.builder(
                itemCount: state.list.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) => RecipientTile(
                      recipient: state.list[index],
                    ));
          else
            return nothingFound(context);
        } else
          return SizedBox();
      },
    );
  }

  Widget nothingFound(BuildContext context) => Container(
        padding: EdgeInsets.all(Values.tilePadding),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Values.borderRadius * 0.8),
          color:
              Theme.of(context).errorColor.withOpacity(Values.containerOpacity),
        ),
        child: Text(
          "No data found. Please revise your query.",
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                color: Theme.of(context).errorColor,
              ),
        ),
      );

  Widget nothingSearched(BuildContext context) => Container(
      padding: const EdgeInsets.all(25),
      decoration: BoxDecoration(color: TileColors.expansionTile(context)),
      child: Text("You haven't searched for anything"));
}
