import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:oolab_test/UI/View/Search/widget/results_list.dart';
import 'package:oolab_test/UI/View/Search/widget/text_box.dart';
import 'package:oolab_test/UI/View/Widgets/page_template.dart';
import 'package:oolab_test/UI/View/Widgets/space.dart';
import 'package:oolab_test/UI/View/Widgets/title_text.dart';
import 'package:oolab_test/UI/styles/values.dart';
import 'package:oolab_test/UI/utils/extensions.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      children: [
        TitleText(
                title: "search".toUpperCase(),
                iconData: FontAwesomeIcons.search)
            .sliverToBoxAdapter(),

        // search bar floats on top when you scroll down then scroll back up
        SliverPersistentHeader(
            pinned: false,
            floating: true,
            delegate: SearchBoxPersistentHeaderDelegate()),

        Spacing(height: Values.marginBelowTitle).sliver(),

        SearchResultsList().sliverToBoxAdapter()
      ],
    ).scaffold();
  }
}

class SearchBoxPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  SearchBoxPersistentHeaderDelegate();

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SearchTextBox();
  }

  @override
  double get maxExtent => 70;

  @override
  double get minExtent => 70;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
