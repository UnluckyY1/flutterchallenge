import 'package:flutter/material.dart';

class TileColors {
  static Color expansionTile(context) => Theme.of(context).brightness == Brightness.light
      ? Colors.transparent.withOpacity(0.03)
      : Color(0xff0b0b0b);

  static Color serviceTile(context) => Theme.of(context).brightness == Brightness.light
      ? Colors.transparent.withOpacity(0.025)
      : Colors.black;
}
