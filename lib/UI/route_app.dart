import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:oolab_test/UI/View/MainScreen.dart';
import 'package:oolab_test/UI/styles/theme.dart';
import 'package:oolab_test/UI/utils/bounce_scroll.dart';
import 'package:oolab_test/UI/utils/theme_enum.dart';

class RouteApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<dynamic>>(
      // to change theme
      valueListenable: Hive.box('settings').listenable(),
      builder: (context, box, widget) {
        final theme =
            box.get('theme', defaultValue: ThemeEnum.light) ?? ThemeEnum.light;
        ThemeData regTheme;
        regTheme = theme == ThemeEnum.light ? appLightTheme : appDarkTheme;

        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'oolab',
          theme: regTheme,
          // builder here to get context from MaterialApp
          home: Builder(
            builder: (BuildContext context) {
              return ScrollConfiguration(
                child: ValueListenableBuilder(
                  builder: (context, box, widget) {
                    return MainScreen();
                  },
                  valueListenable: Hive.box('settings').listenable(),
                ),
                behavior: BounceScrollBehavior(),
              );
            },
          ),
        );
      },
    );
  }
}
