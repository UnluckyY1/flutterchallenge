import 'package:url_launcher/url_launcher.dart';

void openUrl(String url) async {
  if (await canLaunch(url))
    await launch(url);
  else
    throw 'Could not launch $url';
}

void openmail(String url) async {
  // if (await canLaunch('mailto$url'))
    await launch('mailto:$url');
  // else
  //   throw 'Could not launch $url';
}

void openphonecall(String url) async {
  if (await canLaunch('tel:$url'))
    await launch('tel:$url');
  else
    throw 'Could not launch $url';
}
