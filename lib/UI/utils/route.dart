import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oolab_test/UI/utils/bounce_scroll.dart';

class Routing {
  // no going back allowed
  static void openReplacementRoute(BuildContext context, Widget page) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (BuildContext context) => ScrollConfiguration(
          child: page,
          behavior: BounceScrollBehavior(),
        ),
      ),
    );
  }

  static void closeRoute(BuildContext context) => Navigator.of(context).pop();
}
