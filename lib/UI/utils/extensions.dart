import 'package:flutter/material.dart';


// extension on widget class
extension E on Widget {
  Widget sliverToBoxAdapter() {
    return SliverToBoxAdapter(child: this);
  }
}