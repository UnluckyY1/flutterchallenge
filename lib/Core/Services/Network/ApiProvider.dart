import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:oolab_test/Core/consts/keys.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'network_exceptions.dart';

class ApiProvider {
  // API base config
  static final String baseUrl = "https://apiqacss.calindasoftware.com/api/v4/";

  static final BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 10000,
      receiveTimeout: 10000,
      method: 'GET',
      headers: {"j_token": Keys.jtoken},
      contentType: Headers.jsonContentType);

  static final Dio dio = new Dio(options);

  /// get recipientsList
  Future<List<dynamic>> getrecipientsList() async {
    /// debug the output from api for testing
    // dio.interceptors.add(PrettyDioLogger());
    try {
      /// requests data
      final res = await dio.get("/recipients", queryParameters: {
        "size": 100,
      });

      /// return Response body
      return res.data as List;
    } catch (e) {
      ///throws dio/Network Exception as String
      return throw NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }
  }

  Future<dynamic> createRecipient(Map<String, Object?> recipient) async {
    /// debug the output from api for testing
    dio.interceptors.add(PrettyDioLogger());
    try {
      /// requests data
      final res = await dio.post("/recipients", data: json.encode(recipient));

      /// return Response body
      return res.data ;
    } catch (e) {
      ///throws dio/Network Exception as String
      return throw NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }
  }
}
