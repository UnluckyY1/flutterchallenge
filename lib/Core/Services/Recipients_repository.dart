import 'package:connectivity/connectivity.dart';
import 'package:hive/hive.dart';
import 'package:oolab_test/Core/Services/Network/ApiProvider.dart';
import 'package:oolab_test/Core/Services/local/db_provider.dart';
import 'package:oolab_test/Core/model/Recipients.dart';

class Repository {
  final DBProvider _db = DBProvider();
  final ApiProvider _api = ApiProvider();

  // get All Recipients List from database
  Future<List<Recipients>> getAllRecipientsList() async =>
      await _db.getAllRecipients();

  //search Recipients from database
  Future<List<Recipients>> searchRecipients(String keyword) async =>
      await _db.searchRecipients(keyword);

  Future<void> createRecipient(Map<String, Object> recipient) async {
    try {
      await _api.createRecipient(recipient);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<void> fetchdata() async {
    // check if this is the first time using the app
    final settingsBox = Hive.box('settings');
    final bool firstLaunch =
        settingsBox.get('first_launch', defaultValue: true);
    final bool connectivity = await checkConnectivity();

    if (!connectivity && firstLaunch)
      throw "You need to be connected to the internet at the first launch";
    else if (connectivity) await fetchdataFromNetwork();
  }

  Future fetchdataFromNetwork() async {
    try {
      /// gets data from Network
      final data = await _api.getrecipientsList();
      final List<Recipients> recipients =
          data.map<Recipients>((item) => Recipients.fromJson(item)).toList();

      /// Saves data to the local database
      _db.cacheRecipient(recipients);

      /// Setting first launch to false after caching data to the local
      final settingsBox = Hive.box('settings');
      settingsBox.put('first_launch', false);
    } catch (e) {
      print(e);
      throw e;
    }
  }

// check if device is conncted to mobile data OR wifi
  Future<bool> checkConnectivity() async {
    final connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi)
      return true;
    else
      return false;
  }
}
