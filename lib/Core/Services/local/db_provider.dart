import 'dart:io';
import 'package:oolab_test/Core/model/Recipients.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database? _database;
  //static final DBProvider db = DBProvider._();

  //DBProvider._();

  Future<Database?> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();

    return _database;
  }

  // Create the database and the Recipients table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'recipients_db.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Recipients('
          'id INTEGER PRIMARY KEY,'
          'civility TEXT,'
          'firstname TEXT,'
          'lastname TEXT,'
          'address_1 TEXT,'
          'address_2 TEXT,'
          'postal_code TEXT,'
          'city TEXT,'
          'cell_phone TEXT,'
          'email TEXT'
          ')');
    });
  }

// Insert list of Recipients on database
  cacheRecipient(List<Recipients> list) async {
    // await deleteAllRecipients();
    list.forEach((recipient) async {
      await createRecipients(recipient);
    });
  }

  // Insert Recipient on database
  Future createRecipients(Recipients recipient) async {
    final db = await database;
    final res = await db!.insert('Recipients', recipient.toJson(),

        /// [ConflictAlgorithm.replace] as isnert/update to avoid conflicts if id exists on database
        conflictAlgorithm: ConflictAlgorithm.replace);

    return res;
  }

  // Delete all Recipients
  Future<int?> deleteAllRecipients() async {
    final db = await database;
    final res = await db!.rawDelete('DELETE FROM Recipients');

    return res;
  }

  //  get All Recipients from database
  Future<List<Recipients>> getAllRecipients() async {
    try {
      final db = await database;
      final res = await db!.rawQuery("SELECT * FROM Recipients");

      List<Recipients> list =
          res.isNotEmpty ? res.map((c) => Recipients.fromJson(c)).toList() : [];

      return list;
    } catch (e) {
      print(e);
      throw e;
    }
  }

  //  search for Recipients from database by firstname,lastname OR email
  Future<List<Recipients>> searchRecipients(String keyword) async {
    final db = await database;
    final res = await db!.rawQuery(
        "SELECT * FROM Recipients WHERE firstname LIKE '%$keyword%' OR lastname LIKE '%$keyword%' OR email LIKE '%$keyword%'");

    List<Recipients> list =
        res.isNotEmpty ? res.map((c) => Recipients.fromJson(c)).toList() : [];

    return list;
  }
}
