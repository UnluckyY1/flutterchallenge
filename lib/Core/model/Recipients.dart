class Recipients {
  late int id;
  late String civility;
  late String firstname;
  late String lastname;
  late String address1;
  late String address2;
  late String postalCode;
  late String city;
  late String email;
  late String cellPhone;

  Recipients.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    civility = json['civility'];
    firstname = json['firstname'] ?? "";
    lastname = json['lastname'] ?? "";
    address1 = json['address_1'] ?? "";
    address2 = json['address_2'] ?? "";
    postalCode = json['postal_code'] ?? "";
    city = json['city'] ?? "";
    email = json['email'] ?? "";
    cellPhone = json['cell_phone'] ?? "";
  }

  Map<String, Object?> toJson() {
    final Map<String, Object> data = new Map<String, Object>();
    data['id'] = this.id;
    data['civility'] = this.civility;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['address_1'] = this.address1;
    data['address_2'] = this.address2;
    data['postal_code'] = this.postalCode;
    data['city'] = this.city;
    data['email'] = this.email;
    data['cell_phone'] = this.cellPhone;

    return data;
  }
}
