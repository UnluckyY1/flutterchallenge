part of 'Create_cubit.dart';

@immutable
abstract class CreateState {
  const CreateState();
}

class Initial extends CreateState {
  const Initial();
}

class Loading extends CreateState {
  const Loading();
}

class Created extends CreateState {
  const Created();
}

class Error extends CreateState {
  final String message;
  const Error(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Error && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
