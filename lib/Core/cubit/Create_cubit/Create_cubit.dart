import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:oolab_test/Core/Services/Recipients_repository.dart';

part 'Create_state.dart';

class CreateCubit extends Cubit<CreateState> {
  final Repository _repository;

  CreateCubit(this._repository) : super(Initial());

  Future<void> createRecipient(Map<String, Object> recipient) async {
    try {
      //Set state to loading
      emit(Loading());
      // gets data from the local database
      await _repository.createRecipient(recipient);
      // update the local database
      await _repository.fetchdataFromNetwork();
      //Set state to created
      emit(Created());
    } catch (e) {
      print(e);
      //Set state to the catched error
      emit(Error(e.toString()));
    }
  }
}
