import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:oolab_test/Core/Services/Recipients_repository.dart';
import 'package:oolab_test/Core/model/Recipients.dart';

part 'Search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  final Repository _repository;

  SearchCubit(this._repository) : super(Initial());

  Future<void> searchRecipients(String keyword) async {
    try {
      emit(Searching());
      final list = await _repository.searchRecipients(keyword);
      emit(Loaded(list));
    } catch (e) {
      print(e);
      emit(Error("Couldn't fetch data. Is the device online?"));
    }
  }
}
