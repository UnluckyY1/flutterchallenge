part of 'Search_cubit.dart';

@immutable
abstract class SearchState {
  const SearchState();
}

// Intial State
class Initial extends SearchState {
  const Initial();
}

// Searching State
class Searching extends SearchState {
  const Searching();
}

class Loaded extends SearchState {
  final List<Recipients> list;
  const Loaded(this.list);
}

class Error extends SearchState {
  final String message;
  const Error(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Error && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
