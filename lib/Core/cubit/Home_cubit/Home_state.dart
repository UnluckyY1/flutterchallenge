part of 'Home_cubit.dart';

@immutable
abstract class HomeState {
  const HomeState();
}

class Initial extends HomeState {
  const Initial();
}

class Loading extends HomeState {
  const Loading();
}

class Loaded extends HomeState {
  final List<Recipients> list;
  const Loaded(this.list);
}

class Error extends HomeState {
  final String message;
  const Error(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Error && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
