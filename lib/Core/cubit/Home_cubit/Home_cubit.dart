import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:oolab_test/Core/Services/Recipients_repository.dart';
import 'package:oolab_test/Core/model/Recipients.dart';

part 'Home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  final Repository _repository;

  HomeCubit(this._repository) : super(Initial());

  Future<void> fetchdata() async {
    try {
      //Set state to loading
      emit(Loading());
      // gets data from the local database
      final list = await _repository.getAllRecipientsList();
      //Set state to loaded with the result list
      emit(Loaded(list));
    } catch (e) {
      print(e);
      emit(Error(e.toString()));
    }
  }

  Future<void> refrechdata() async {
    try {
      //Set state to loading
      emit(Loading());
      // gets data from the network and update the local database
      _repository.fetchdata();
      //gets data from the local database
      final list = await _repository.getAllRecipientsList();
      //Set state to loaded with the result list
      emit(Loaded(list));
    } catch (e) {
      print(e);
      emit(Error("Couldn't fetch data. Is the device online?"));
    }
  }
}
