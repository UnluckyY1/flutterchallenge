part of 'Main_cubit.dart';

@immutable
abstract class AppState {
  const AppState();
}

class Initial extends AppState {
  const Initial();
}

class Loading extends AppState {
  const Loading();
}

class Loaded extends AppState {
  const Loaded();
}

class Error extends AppState {
  final String message;
  const Error(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Error && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
