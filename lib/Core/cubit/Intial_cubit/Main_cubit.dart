import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:oolab_test/Core/Services/Recipients_repository.dart';

part 'App_state.dart';

class MainCubit extends Cubit<AppState> {
  final Repository _repository;

  MainCubit(
    this._repository,
  ) : super(Initial());

  Future<void> fetchdata() async {
    try {
      emit(Loading());
      await _repository.fetchdata();
      emit(Loaded());
    } catch (e) {
      print(e);
      emit(Error(e.toString()));
    }
  }
}
