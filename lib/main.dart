import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:oolab_test/Core/Services/Recipients_repository.dart';
import 'package:oolab_test/Core/cubit/Home_cubit/Home_cubit.dart';
import 'package:oolab_test/Core/cubit/Intial_cubit/Main_cubit.dart';
import 'package:oolab_test/Core/cubit/Search_cubit/Search_cubit.dart';
import 'package:oolab_test/UI/route_app.dart';
import 'package:oolab_test/UI/utils/theme_enum.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(ThemeEnumAdapter());
  await Hive.openBox('settings');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Repository repository = Repository();
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => MainCubit(repository),
        ),
        BlocProvider(
          create: (context) => HomeCubit(repository),
        ),
        BlocProvider(
          create: (context) => SearchCubit(repository),
        ),
      ],
      child: RouteApp(),
    );
  }
}
